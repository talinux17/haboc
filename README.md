# HaBoC

Hacker Bob in C.  
Demo for a rpm gone rogue. 
The idea behind it is an user having sudo rights for yum only. Then he can install an rpm that will bring the executable and it will use SUID bit to make the dirty work.
```bash
chmod u+s haboc
```

Compile it with:
`gcc haboc.c -o haboc`